class Developer {
  var devname;
  var devyear;
  var devapp;
  var devcategory;
  
    showDevInfo() {
    print("Developer name is : ${devname}");
    print("Year app Won is : ${devyear}");
    print("The app category is : ${devcategory}");
    print("The name is the app is : ${devapp}");
  }

   String get inCaps => '${devapp[0].toUpperCase()}';
}

void main() {
  var dev = Developer();
  dev.devname = "Charles Savage";
  dev.devyear = 2020;
  dev.devapp = "EasyEquities";
  dev.devcategory = "Overall Winner";
  dev.showDevInfo();

    print(dev.devapp.toUpperCase());

}
